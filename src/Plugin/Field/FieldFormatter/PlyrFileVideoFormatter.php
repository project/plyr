<?php

namespace Drupal\plyr\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'plyr_file_video' formatter.
 *
 * @FieldFormatter(
 *   id = "plyr_file_video",
 *   label = @Translation("Plyr for video files"),
 *   field_types = {
 *     "file",
 *   },
 * )
 */
class PlyrFileVideoFormatter extends PlyrFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'video';
  }

}
