<?php

namespace Drupal\plyr\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Shared trait for Plyr formatters.
 */
trait PlyrSharedFormatterTrait {

  use StringTranslationTrait;

  /**
   * Attach the correct libraries for this field instance.
   *
   * @param array $element
   *   The element array.
   * @param int $delta
   *   The delta.
   */
  public function attachPlyrLibraries(array &$element, int $delta): void {
    $element[$delta]['#plyr_settings'] = $this->buildPlyrDrupalSettings();
  }

  /**
   * Build Plyr drupalSettings.
   *
   * Build the settings array that is passed to the Twig template
   * to build up the data-attributes per player instance.
   *
   * @return array
   *   Return the Plyr settings array.
   */
  public function buildPlyrDrupalSettings() {
    $settings = [];
    foreach ($this->settings as $settingName => $settingValue) {
      if ($settingName === 'controls') {
        $controls = [];
        foreach ($settingValue as $controlName => $controlValue) {
          if (((bool) $controlValue)) {
            $controls[] = $controlName;
          }
        }
        $settings['controls'] = $controls;
      }
      elseif ($settingName === 'youtube') {
        $youTube = new \stdClass();
        foreach ($settingValue as $controlName => $controlValue) {
          if (((bool) $controlValue)) {
            $youTube->{$controlName} = (bool) $controlValue;
          }
        }
        $settings['youtube'] = $youTube;
      }
      elseif ((bool) $settingValue) {
        $settings[$settingName] = TRUE;
      }
    }
    return $settings;
  }

  /**
   * Default settings for the formatter.
   *
   * @return array
   *   Return the default settings array.
   */
  public static function defaultSettings() {
    return [
      'autoplay' => FALSE,
      'loop' => FALSE,
      'resetOnEnd' => TRUE,
      'hideControls' => TRUE,
      'controls' => [
        'play-large' => FALSE,
        'restart' => FALSE,
        'rewind' => FALSE,
        'play' => TRUE,
        'fast-forward' => FALSE,
        'progress' => TRUE,
        'current-time' => TRUE,
        'duration' => FALSE,
        'mute' => TRUE,
        'volume' => TRUE,
        'captions' => FALSE,
        'settings' => TRUE,
        'pip' => FALSE,
        'airplay' => FALSE,
        'fullscreen' => TRUE,
      ],
      'youtube' => [
        'noCookie' => TRUE,
      ],
    ];
  }

  /**
   * The settings form for the formatter.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Return settings form array.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    // General settings.
    $form['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#description' => $this->t('Autoplay the media on load. This is generally advised against on UX grounds. It is also disabled by default in some browsers.'),
      '#default_value' => $settings['autoplay'],
    ];
    $form['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#description' => $this->t('Loop the media.'),
      '#default_value' => $settings['loop'],
    ];
    $form['resetOnEnd'] = [
      '#title' => $this->t('Reset on end'),
      '#type' => 'checkbox',
      '#description' => $this->t('Reset the playback to the start once playback is complete.'),
      '#default_value' => $settings['resetOnEnd'],
    ];
    $form['hideControls'] = [
      '#title' => $this->t('Hide controls'),
      '#type' => 'checkbox',
      '#description' => $this->t('Hide video controls automatically after 2s of no mouse or focus movement, on control element blur (tab out), on playback start or entering fullscreen.'),
      '#default_value' => $settings['hideControls'],
    ];

    // Controls settings.
    $form['controls'] = [
      '#title' => $this->t('Controls'),
      '#description' => $this->t('Configure the Plyr player controls.'),
      '#type' => 'fieldset',
    ];
    $form['controls']['play-large'] = [
      '#title' => $this->t('Large play button'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show a large centered play button.'),
      '#default_value' => $settings['controls']['play-large'],
    ];
    $form['controls']['play'] = [
      '#title' => $this->t('Play button'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show play/pause button.'),
      '#default_value' => $settings['controls']['play'],
    ];
    $form['controls']['restart'] = [
      '#title' => $this->t('Restart button'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show restart button.'),
      '#default_value' => $settings['controls']['restart'],
    ];
    $form['controls']['rewind'] = [
      '#title' => $this->t('Rewind button'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show a rewind button (Plyr default: 10 seconds).'),
      '#default_value' => $settings['controls']['rewind'],
    ];
    $form['controls']['fast-forward'] = [
      '#title' => $this->t('Fast forward button'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show a fast forward button (Plyr default: 10 seconds).'),
      '#default_value' => $settings['controls']['fast-forward'],
    ];
    $form['controls']['progress'] = [
      '#title' => $this->t('Progress bar'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show progress bar.'),
      '#default_value' => $settings['controls']['progress'],
    ];
    $form['controls']['current-time'] = [
      '#title' => $this->t('Current time'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show current time of playback.'),
      '#default_value' => $settings['controls']['current-time'],
    ];
    $form['controls']['duration'] = [
      '#title' => $this->t('Duration'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show duration of media if available.'),
      '#default_value' => $settings['controls']['duration'],
    ];
    $form['controls']['mute'] = [
      '#title' => $this->t('Mute button'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show toggle mute button.'),
      '#default_value' => $settings['controls']['mute'],
    ];
    $form['controls']['volume'] = [
      '#title' => $this->t('Volume controls'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show volume controls.'),
      '#default_value' => $settings['controls']['volume'],
    ];
    $form['controls']['settings'] = [
      '#title' => $this->t('Settings controls'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show available settings controls.'),
      '#default_value' => $settings['controls']['settings'],
    ];
    $form['controls']['airplay'] = [
      '#title' => $this->t('Airplay'),
      '#type' => 'checkbox',
      '#description' => $this->t('Allow airplay playback.'),
      '#default_value' => $settings['controls']['airplay'],
    ];
    $form['controls']['pip'] = [
      '#title' => $this->t('Picture-in-Picture'),
      '#type' => 'checkbox',
      '#description' => $this->t('Allow picture-in-picture playback (limited browser support).'),
      '#default_value' => $settings['controls']['pip'],
    ];
    $form['controls']['fullscreen'] = [
      '#title' => $this->t('Fullscreen'),
      '#type' => 'checkbox',
      '#description' => $this->t('Allow fullscreen playback.'),
      '#default_value' => $settings['controls']['fullscreen'],
    ];

    // YouTube specific settings.
    // @todo Should be configured via module settings form.
    $form['youtube'] = [
      '#title' => $this->t('YouTube settings'),
      '#type' => 'fieldset',
    ];
    $form['youtube']['noCookie'] = [
      '#title' => $this->t('Use the noCookie domain'),
      '#type' => 'checkbox',
      '#description' => $this->t('Use the noCookie domain for YouTube playbacks. Attention: This configuration will maybe moved in near future to Plyr module settings!'),
      '#default_value' => $settings['youtube']['noCookie'],
    ];

    return $form;
  }

  /**
   * Summary of the form widget settings.
   *
   * @return array
   *   The summary of the settings.
   */
  public function settingsSummary() {
    $settings = $this->getSettings();

    $generalSettings = [];
    if (TRUE === (bool) $settings['autoplay']) {
      $generalSettings[] = $this->t('Autoplaying');
    }
    if (TRUE === (bool) $settings['loop']) {
      $generalSettings[] = $this->t('Looping');
    }
    if (TRUE === (bool) $settings['resetOnEnd']) {
      $generalSettings[] = $this->t('Reset on end');
    }
    if (TRUE === (bool) $settings['hideControls']) {
      $generalSettings[] = $this->t('Hide controls automatically');
    }

    $controlsSettings = [];
    foreach ($settings['controls'] as $name => $value) {
      if (TRUE === (bool) $value) {
        $controlsSettings[] = $this->t('@control', ['@control' => ucfirst($name)]);
      }
    }

    $summary = [];
    $summary[] = $this->t('General: @general', ['@general' => implode(', ', $generalSettings)]);
    $summary[] = $this->t('Control: @controls', ['@controls' => implode(', ', $controlsSettings)]);
    return $summary;
  }

}
