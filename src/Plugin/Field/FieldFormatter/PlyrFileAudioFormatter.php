<?php

namespace Drupal\plyr\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'plyr_file_audio' formatter.
 *
 * @FieldFormatter(
 *   id = "plyr_file_audio",
 *   label = @Translation("Plyr for audio files"),
 *   field_types = {
 *     "file",
 *   },
 * )
 */
class PlyrFileAudioFormatter extends PlyrFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'audio';
  }

}
