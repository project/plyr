CONTENTS OF THIS FILE
---------------------

- Introduction
- Requirements
- Installation
- Configuration
- Image Style Warmer
- Support
- Maintainers


INTRODUCTION
------------

The Plyr module for D8 and D9 currently provides "Plyr for remote videos" 
field formatter plugin for Drupal's core remote video media entity. 

The [Plyr library](https://plyr.io) supports remote videos from
Vimeo and YouTube.


REQUIREMENTS
------------

- Media module from Drupal core

Plyr library is currently added as external library from Fastly CDN.


INSTALLATION
------------

__Versions 8.x-1.x:__ Install as usual, see the [official documentation](https://www.drupal.org/documentation/install/modules-themes/modules-8)
for further information.


CONFIGURATION
-------------

To use Plyr player for your remote video media entities you only have to
configure field formatter to "Plyr for remote videos" instead of
oEmbed content field formatter.

The module provides following field formatters:

- Plyr for remote videos


__This project has been sponsored by:__

- Government of Flanders
- [IT-Cru](https://it-cru.de) 

Image Style Warmer
------------------

Project page: https://drupal.org/project/plyr


Support
-------

File bugs, feature requests and support requests in the [Drupal.org issue queue
of this project](https://www.drupal.org/project/issues/plyr).


Maintainers
-----------

Current maintainers for Plyr:
- [Graber](https://www.drupal.org/u/graber)
- [IT-Cru](https://www.drupal.org/u/IT-Cru)
